package com.pritam.githubexplorer.utils

public class Constants {

    // static constants
    companion object {
        const val BASE_URL: String = "https://api.github.com/"
        const val GIST_URL: String = "https://gist.github.com/"
        const val APP_TAG: String = "GithubExplorer"
        const val EMAIL_VERIFICATION = "^[a-zA-Z0-9_!#\$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+\$"
        const val CONNECTION_TIMEOUT: Long = 30
    }
}